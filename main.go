package main

import (
	"crypto/sha256"
	"flag"
	"github.com/jackmanlabs/bucket/jlog"
	"github.com/jackmanlabs/errors"
	"hash"
	"io"
	"log"
	"os"
	"path/filepath"
	"sync"
)

func main() {

	var (
		source *string = flag.String("src", "", "The source file or directory to transfer.")
		jobs   *int    = flag.Int("jobs", 1, "The number of threads to employ for rsync operations.")
	)

	flag.Parse()

	if *source == "" {
		flag.Usage()
		log.Fatal("Command line flag 'source' is required.")
	}

	if *jobs <= 0 {
		flag.Usage()
		log.Fatal("Command line flag 'jobs' must be a positive integer.")
	}

	descriptors, err := createFileList(*source)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	HashDescriptors(descriptors, *jobs)

	jlog.Log(descriptors)
}

func HashDescriptors(descriptors []Descriptor, jobs int) {

	feed := make(chan *Descriptor)

	wg := new(sync.WaitGroup)
	wg.Add(jobs)

	for i := 0; i < jobs; i++ {
		go DescriptorHasher(feed, wg)
	}

	for k, d := range descriptors {
		if d.IsDirectory {
			log.Print("Skipping directory: ", d.Path)
		} else {
			feed <- &descriptors[k]
		}
	}

	close(feed)

	wg.Wait()
}

func DescriptorHasher(ch chan *Descriptor, wg *sync.WaitGroup) {
	defer wg.Done()

	for descriptor := range ch {
		log.Print("Hashing: ", descriptor.Path)
		err := descriptor.Hash()
		if err != nil{
			log.Print(errors.Stack(err))
		}
	}
}

type Descriptor struct {
	Path        string
	IsDirectory bool
	Checksum    []byte
	Size        int64 // Bytes
}

func (this *Descriptor) Hash() error {

	f, err := os.Open(this.Path)
	if err != nil {
		return errors.Stack(err)
	}
	defer f.Close()

	var hasher hash.Hash = sha256.New()
	n, err := io.Copy(hasher, f)
	if err != nil {
		return errors.Stack(err)
	}

	this.Size = n
	this.Checksum = hasher.Sum(nil)

	return nil
}

func createFileList(path string) ([]Descriptor, error) {

	path, err := filepath.Abs(path)
	if err != nil {
		return nil, errors.Stack(err)
	}

	f, err := os.Open(path)
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer f.Close()

	stat, err := f.Stat()
	if err != nil {
		return nil, errors.Stack(err)
	}

	var desc Descriptor
	desc.Path = path

	if !stat.IsDir() {
		desc.Size = stat.Size()
		return []Descriptor{desc}, nil
	}

	desc.IsDirectory = true
	descriptors := []Descriptor{desc}

	children, err := f.Readdirnames(-1)
	path += "/"
	for _, child := range children {
		descriptors_, err := createFileList(path + child)
		if err != nil {
			return nil, errors.Stack(err)
		}
		descriptors = append(descriptors, descriptors_...)
	}

	return descriptors, nil
}
